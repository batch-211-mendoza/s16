console.log("Hello World");

// Arithmetic Operators

	let x = 1397;
	let y = 7831;

	let sum = x + y;
	console.log("Result of addition operator: "+ sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product = x * y;
	console.log("Result of multiplication operator: " + product);

	let quotient = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

// Assignment Operators

	// • Basic Assignment Operator (=)
	let assignmentNumber = 8;

	// • Addition Assignment Operator (+=)
	// Addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable
	assignmentNumber = assignmentNumber + 2; //basic assignment operator
	console.log("Result of addition operator: " + assignmentNumber);

	assignmentNumber += 2; //addition assignment operator
	console.log("Result of addition assignment operator: " + assignmentNumber);

	// • Subtraction Assignment Operator (-=)
	// assignmentNumber = assignmentNumber - 2; //basic assignment operator
	// console.log("Result of subtraction operator: " + assignmentNumber);

	assignmentNumber -= 2; //subtraction assignment operator
	console.log("Result of subtraction assignment operator: " + assignmentNumber);

	// • Multiplication Assignment Operator (*=)
	// assignmentNumber = assignmentNumber * 2; //basic assignment operator
	// console.log("Result of multiplication operator: " + assignmentNumber);

	assignmentNumber *= 2; //multiplication assignment operator
	console.log("Result of multiplication assignment operator: " + assignmentNumber);

	// • Division Assignment Operator (/=)
	// assignmentNumber = assignmentNumber / 2; //basic assignment operator
	// console.log("Result of division operator: " + assignmentNumber);

	assignmentNumber /= 2; //division assignment operator
	console.log("Result of division assignment operator: " + assignmentNumber);

// Multiple Operators and Parentheses
	/*
		-When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition, and Subtraction) rule
	*/

	let mdas = 1 + 2 - 3 * 4 / 5;
	console.log("Result of mdas operation: " + mdas); //output: 0.6000000000000001
	/*
		1. 3 * 4 = 12
		2. 12 / 5 = 2.4
		3. 1 + 2 = 3
		4. 3 - 2.4 = 0.6
	*/

	let pemdas = 1 + (2 - 3) * (4 / 5);
	console.log("Result of pemdas operation: " + pemdas); //output: 0.19999999999999996 or 2
	/*
		-By adding parentheses "()" to create more complex computations will change the order of operations still following the same rule
	*/
	/*
		1. 4 / 5 = 0.8
		2. 2 - 3 = -1
		3. -1 * 0.8 = -0.8
		4. 1 + -0.8 = 0.2
	*/

// Increement and Decrement
	// Operators that add or subtract values by 1 and reassigns the value of the variable where the incremenet/decrement was applied to

	let z = 1;
	let increment = ++z; // the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
	console.log("Result of pre-increment: " + increment); //output: 2
	console.log("Result of pre-increment: " + z); //output: 2


	// The value of "z" variable is returned and stored in the variable "increment" then the value of "z" is increased by 1
	increment = z++;

	// The value of "z" is at '2' before it was incremented
	console.log("Result of post-increment: " + increment); //output: 2
	// The value of "z" was increased again reassigning the value to 3
	console.log("Result of post-increment: " + z); //output: 3

	let decrement = --z; // the value of "z" is subtracted by a value of one before returning the value and storing it in the variable "increment"
	console.log("Result of pre-decrement: " + increment); //output: 2
	console.log("Result of pre-decrement: " + z); //output: 2

	// The value of "z" variable is returned and stored in the variable "increment" then the value of "z" is decreased by 1
	decrement = z--;

	// The value of "z" is at '2' before it was decremented
	console.log("Result of post-decrement: " + decrement); //output: 2
	// The value of "z" was decreased again reassigning the value to 1
	console.log("Result of post-decrement: " + z); //output: 1

	//Type Coercion
	let numA = "10"; //data type String
	let numB = 12; //data type Number
	/*
		- adding/concatenating a string and a number will result in a String
		*this can be proven in the console by looking at the color of the text displayed.
	*/
	let coercion = numA + numB;
	console.log(coercion);
	console.log(typeof coercion);

	let numC = 16;
	let numD = 14;
	/*
		- the result is a number
		*this can be proven in our console by looking at the color of the text displayed
			- blue/purple text means that the output returned is a Number data type
	*/
	let nonCoercion = numC + numD;
	console.log(nonCoercion);
	console.log(typeof nonCoercion);

	/*
		-the result is a number
		-the boolean "true" is also associated with the value of 1
	*/
	let numE = true + 1;
	console.log(numE);

	/*
		-the result is a number
		-the boolean "false" is also associated with the value of 0
	*/
	let numF = false + 1;
	console.log(numF);

// Comparison Operators
	
	let juan = "juan";

	// • Equality Operator (==)
	/*
		-checks whether the operands are equal/have the same content
		-attempts to CONVERT and COMPARE operands of different data types
		-returns a boolean value (true/false) 
	*/
	console.log(1 == 1); //output: true
	console.log(1 == 2); //output: false
	console.log(1 == '1'); //output: true
	console.log('1' == 1); //output: true
	console.log(0 == false); //output: true
	console.log("juan" == "juan"); //output: true
	console.log("juan" == juan); //output: true

	// • Inequality Operator (!=)
	/*
		-it checks whether the operands are not equal/have different content
		-attempts to CONVERT and COMPARE operands fo different data types
	*/
	console.log(1 != 1); //output: false
	console.log(1 != 2); //output: true 
	console.log(1 != '1'); //output: false
	console.log('1' != 1); //output: false
	console.log(0 != false); //output: false
	console.log("juan" != "juan"); //output: false
	console.log("juan" != juan); //output: false

	// • Strict Equality Operator (===)
	/*
		-checks whether the operands are equal/have the same content
		-also, it COMPARES the data types of 2 values
		-JavaScript is a loosely-typed language meaning that values of different data types can be stored in variables
		-strict equality operators are better to use in most cases to ensure the data types are correct
	*/
	console.log(1 === 1); //output: true
	console.log(1 === 2); //output: false
	console.log(1 === '1'); //output: false
	console.log('1' === 1); //output: false
	console.log(0 === false); //output: false
	console.log("juan" === "juan"); //output: true
	console.log("juan" === juan); //output: true

	// • Strict Inequality Operator (!==)
	/*
		-checks whether the operands are not equal/have the same content
		-also COMPARES the data types of 2 values
	*/
	console.log(1 !== 1); //output: false
	console.log(1 !== 2); //output: true 
	console.log(1 !== '1'); //output: true
	console.log('1' !== 1); //output: true
	console.log(0 !== false); //output: true
	console.log("juan" !== "juan"); //output: false
	console.log("juan" !== juan); //output: false

	// • Relational Operators

	let a = 50;
	let b = 65;

	let isGreaterThan = a > b;
	let isLessThan = a < b;
	let isGTorEqual = a >= b;
	let isLTorEqual = a <= b;

	console.log(isGreaterThan); //output: false
	console.log(isLessThan); //output: true
	console.log(isGTorEqual); //output: false
	console.log(isLTorEqual); //output: true

	let numString = "30";
	// relational operators will invoke type coercion
	// coercion to change the string to a number
	console.log(a > numString); //output: true
	console.log(b <= numString); //output: false

	let string = "twenty";
	console.log(b >= string); //output: false
	// since the string is not numeric
	// the string was converted to a number
	// result to NaN. 65 is not greater than NaN

	// • Logical Operators (&&, ||)

	let isLegalAge = true;
	let isRegistered = false;

		// Logical AND operator (&& - Double Ampersand)
		// Return TRUE if all operands are true
	let allRequirementsMet = isLegalAge && isRegistered;
	console.log("Result of logical 'AND' operator: " + allRequirementsMet);
	//output: Result of logical 'AND' operator: false

		// Logical OR Operator (|| - Double Pipe)
		//Return TRUE if one of the operands are true
	let someRequirementsMet = isLegalAge || isRegistered;
	console.log("Result of logical OR operator: " + someRequirementsMet);
	//output: output: Result of logical 'AND' operator: true

		// • Logical Not Operator (! - Exclamation Point)
		// returns the opposite value
	let someRequirementsNotMet = !isRegistered;
	console.log("Result of Logical NOT Operator: " + someRequirementsNotMet);
	//output: output: Result of logical 'AND' operator: true

		// • "typeof" Operator is used to check the data type of a value/expression and returns a string value of what the data type is